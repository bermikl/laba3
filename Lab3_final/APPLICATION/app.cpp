#include "stdafx.h"
#include "..\LIBRARY\university.h"

#include <iostream>
#include <string>
#include <exception>

using namespace std;
using namespace lib;

string msgs[] = { "Exit", "Print menu", "Print all information", "Add group", "Add student", "Change group", "Input grades",
"Get average grade", "Change senior", "Get info about bad marks", "Save to file", "Read from file" };
const int amount = 12;

University CreateNew();							//�������� ������ 

int Exit(University&);							//������� ������
int Menu(University&);							//����
int Print(University&);							//����� ���� ���������� � ������� � ���������
int AddGroup(University&);						//�������� ������
int AddStudent(University&);					//�������� ��������
int GhangeGroup(University&);					//�������� ������� ������ � ��������
int InputGrades(University&);					//������ ������ � ���������
int AverageGrade(University&);					//������� ������� ���� �� ������
int ChangeSenior(University&);					//�������� ����, ����� � ������ � ��������������
int BadMarks(University&);						//�������� ���������� � ���� ����������
int SaveFile(University&);						//��������� � ����
int ReadFile(University&);						//�������� �� �����

int getInt();									//������� ���������� ������ �����

int main()
{
	University U;
	try
	{
		U = CreateNew();
	}
	catch (bad_alloc)												//�������� ����������, ���������� � ��������� ������
	{
		cout << "Out of memory. :-(" << endl;
		return 1;
	}
	int ex = 0;														//��������� ��� �������� ������ ����
	char choice = 0;												//����� �� ����� - ����� ����
	for (int i = 0; i<amount; ++i)
		cout << (char)(i + 'a') << " " << msgs[i] << endl;
	int(*func[amount]) (University&) = { Exit, Menu, Print, AddGroup, AddStudent, GhangeGroup, InputGrades, AverageGrade, ChangeSenior, BadMarks, SaveFile, ReadFile };					//������ �������

	while (!ex)
	{																//���� ������ ������
		cout << "\nMake your choice: ";
		cin >> choice;
		while (!((choice>('a' - 1)) && ((choice<amount + 'a'))))	//���� �� ����� ���������� ����� ������
			cin >> choice;
		if ((choice>('a' - 1)) && ((choice<amount + 'a')))
			try
		{
			ex = (*func[choice - 'a'])(U);							//������� ��������, ������� ������� ��������
		}
		catch (bad_alloc)
		{
			ex = 1;
			cout << "Out of memory!" << endl;
		}
	}

	return 0;
}


University CreateNew()														//������� ����� ����� � �������� ������������
{
	University U = University();
	return U;
}

int Exit(University& U) 													//���������� ���������
{
	return 1;
}

int Menu(University&)														//����� ����
{
	int i;
	printf("\n");
	for (i = 0; i < amount; ++i)
		cout << (char)(i + 'a') << " " << msgs[i] << ", ";
	return 0;
}

int Print(University& U)
{
	cout << "University:" << endl;											//����� ������������
	cout << U;
	return 0;
}

int AddGroup(University& U)													//�������� ������
{
	cout << "Enter name of group" << endl;
	string group = "";
	cin >> group;															//���� ����� ������
	cout << "Enter count of grades" << endl;
	int count = getInt();													//����� ������
	cout << "Enter type of students: F - freshmen, S - seniors" << endl;	//���� ��������
	char c = 't';
	cin >> c;
	try
	{
		U.addGroup(group, count, c == 'F' || c == 'f');						//�������� ������
		cout << "Group added" << endl;
	}
	catch (bad_alloc)
	{
		cout << "Out of memory" << endl;									//�������� ���������� � ������ �������� ������
		return 1;
	}
	catch (exception e)
	{
		cout << e.what() << endl;
	}
	return 0;
}

int AddStudent(University& U)												//�������� ��������
{
	cout << "Enter name of student" << endl;
	string name = "";														//���� ����� � ������ ��������
	cin >> name;
	cout << "Enter group" << endl;
	string group = "";
	cin >> group;
	cout << "Enter type of student: F - freshmen, S - seniors" << endl;
	char c = 't';
	cin >> c;																//��� ��������
	try
	{
		if (c == 'F' || c == 'f')
			U.addStudent(group, name);										//������ �������������
		else
		{
			cout << "Enter name of working" << endl;						//���� �� ��������������
			string working = "";
			cin >> working;
			cout << "Enter location" << endl;
			string location = "";
			cin >> location;
			cout << "Enter grade of working" << endl;
			int grade = getInt();
			U.addStudent(group, name, working, location, grade);			//������ ��������������
		}
		cout << "student added" << endl;
	}
	catch (bad_alloc)
	{
		cout << "Out of memory" << endl;									//������ �������� ������
		return 1;
	}
	catch (exception e)
	{
		cout << e.what() << endl;											//������ ����������
	}
	return 0;
}

int GhangeGroup(University& U)												//����� �������� (������)
{
	cout << "Enter name of student" << endl;
	string name = "";														//������ ���, ������ ������, ����� ������
	cin >> name;
	cout << "Enter old group" << endl;
	string groupOld = "";
	cin >> groupOld;
	cout << "Enter new group" << endl;
	string groupNew = "";
	cin >> groupNew;
	int count = U.getStudentsCount(name, groupOld);							//��������� ����� ��������� � ������ � ����������� �������
	if (count == -1)
		cout << "Group not found" << endl;
	else if (count == 0)
		cout << "Student not fount" << endl;
	else
	{
		int index = 0;														//��� ������� ��������� ������ ������ �������������� ����������
		if (count != 1)
		{
			cout << "There are " << count << " " << name << " in group. Change one of them: " << endl;
			cout << U.printStudents(name, groupOld);
			index = getInt();
		}
		try
		{
			U.changeGroup(name, groupOld, groupNew, index);					//�������� ������ ��������
			cout << "Group changed" << endl;
		}
		catch (exception e)
		{
			cout << e.what() << endl;
		}
	}

	return 0;
}

int InputGrades(University& U)												//���� ������ ������
{
	cout << "Enter group" << endl;
	string group = "";
	cin >> group;
	try
	{
		int* grades = new int[U.getGroupCountGrades(group)];				//������ ������
		for (unsigned int i = 0; i < U.sizeGroup(group); i++)
		{
			cout << "Enter grades for this student:" << endl << U.printStudents(i, group) << endl;
			for (unsigned int j = 0; j < U.getGroupCountGrades(group); j++)
			{
				cout << "Enter " << j << " grade" << endl;
				grades[j] = getInt();
			}
			U.setStudentGrades(group, i, grades);							//����� ���������� ������� ������ - �������� ������ ��� ���������������� ��������
		}
		delete[] grades;
		cout << U.printGroup(group);
	}
	catch (exception e)
	{
		cout << e.what() << endl;
	}
	return 0;
}

int AverageGrade(University& U)												//��������� �������� ����� �� ������
{
	cout << "Enter group" << endl;
	string group = "";
	cin >> group;
	try
	{
		cout << "Average grade of " << group << " is " << U.getAverageGrade(group) << endl;	//������� ������� ���� ��� ������
	}
	catch (exception e)
	{
		cout << e.what() << endl;
	}
	return 0;
}

int ChangeSenior(University& U)												//�������� ���������� �� ��������������
{
	cout << "Enter group" << endl;											//���� ��������
	string group = "";
	cin >> group;
	cout << "Enter name of student" << endl;
	string name = "";
	cin >> name;
	cout << "Enter name of working" << endl;								//���� ����� ����������
	string working = "";
	cin >> working;
	cout << "Enter location" << endl;
	string location = "";
	cin >> location;
	cout << "Enter grade of working" << endl;
	int grade = getInt();
	int count = U.getStudentsCount(name, group);							//�������� �� ������ ���������� (�� ���) ��������� � ������
	if (count == -1)
		cout << "Group not found" << endl;
	else if (count == 0)
		cout << "Student not fount" << endl;
	else
	{
		int index = 0;
		if (count != 1)
		{
			cout << "There are " << count << " " << name << " in group. Change one of them: " << endl;
			cout << U.printStudents(name, group);
			index = getInt();
		}
		try
		{
			U.changeInformation(group, name, working, location, grade, index);	//���������������� ��������� ���������� �� ��������
			cout << "Information changed" << endl;
		}
		catch (exception e)
		{
			cout << e.what() << endl;
		}
	}
	return 0;
}

int BadMarks(University& U)													//��������� ���������� �� ���� ��������� - ����������
{
	cout << "Enter group" << endl;
	string group = "";
	cin >> group;															//� ��������� ������
	cout << "Bad marks:" << U.getBadMarks(group);							//������ ������ ������ � ������� ���������, ���������� ��������������
	return 0;
}

int SaveFile(University& U)
{
	cout << "Enter file name" << endl;										//��������� � ����
	string file = "";
	cin >> file;
	try
	{
		U.save(file);
		cout << "File wrote";
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int ReadFile(University& U)
{
	cout << "Enter file name" << endl;										//������� �� �����
	string file = "";
	cin >> file;
	try
	{
		U.read(file);
		cout << "File read";
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int getInt()																//��������� ������ �����, 
{
	int n = 0;
	do																		//���� �� ����� ������ ����������
	{
		cin.clear();														//������� ������ 
		cin.ignore();														//������������� ������
		cin >> n;
	} while (!cin.good());													//���� ����� �� good(), �� ���������� ��������, ������� ����� �����, ��������������� �����
	return n;
}
