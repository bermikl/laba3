#pragma once

#include "group.h"

#include <vector>
#include <iostream>

//������������ ��� ��� ���� ����������
namespace lib
{
	//����� ������������
	class University
	{
	private:
		std::vector<Group*> groups;														//������ �� �����
		Group* find(std::string group) const;											//������� ��� ������ ������ � �������
	public:
		University() : groups(std::vector <Group*>()) {}								//������ �����������
		University(University&&);														//������������
		University(const University&);													//����������
		~University();																	//����������

		University& operator= (University&&);											//������������ �������� ������������
		University& operator= (const University&);										//����������
		friend std::ostream& operator<< (std::ostream&, const University&);				//�������� ������

		void addGroup(std::string group, int count, bool type);							//�������� ������
		void addStudent(std::string group, std::string name);							//�������� �������� ��������
		void addStudent(std::string group, std::string name, int* grades);				//�������� �������� �������� � ���������� ��������
		void addStudent(std::string group, std::string name, std::string working, std::string location, int grade);				//�������� �������������� 
		void addStudent(std::string group, std::string name, std::string working, std::string location, int grade, int* grades);//�������� �������������� � ��������
		int getStudentsCount(std::string name, std::string group) const;				//�������� ����� ��������� � ������ � ����������� ���
		std::string printStudents(std::string name, std::string group) const;			//�������� ������ �� ���������� � ����������� ��� � ����� ������
		void changeGroup(std::string name, std::string groupOld, std::string groupNew, int index);	//�������� ������ ��������
		bool existGroup(std::string group) const { return find(group) != NULL; }		//�������� �� ������������ ������
		unsigned int sizeGroup(std::string group) const;								//������ ������ (���)
		unsigned int getGroupCountGrades(std::string group) const;						//��������� ����� ��������� � ������
		std::string printStudents(unsigned int index, std::string group) const;			//����� �������� �� �������
		void setStudentGrades(std::string group, unsigned int index, int* grades);		//�������� ������ �������� �� �������
		std::string printGroup(std::string group) const;								//������� ���� ������
		double getAverageGrade(std::string group) const;								//�������� ������� ���� �� ����� ������
		void changeInformation(std::string group, std::string name, std::string working, std::string location, int grade, unsigned int index);	//�������� ���������� �� ��������������
		std::string getBadMarks(std::string group) const;								//�������� ������ ���������� � ���, ��� � ��������
		void save(std::string file) const;												//��������� � ����
		void read(std::string file);													//������ �� �����
		void clear();


	};
}
