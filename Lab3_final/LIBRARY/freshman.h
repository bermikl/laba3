#pragma once

#include <string>

namespace lib
{
	class Freshman															//������� �������
	{

	protected:
		std::string name;													//��� ��������
		int count;															//����� ������
		int* grades;														//������

	public:
		Freshman() : Freshman("", 0) {}										//������ �����������
		Freshman(std::string name, int count);								//����������� �� ����� � ���-�� ������
		Freshman(std::string name, int count, int* grades);					//����������� �� �����, ���-�� ������ � ������� ������
		Freshman(Freshman&&);												//������������
		Freshman(const Freshman&);											//� ���������� ����������
		virtual Freshman* clone() { return new Freshman(*this); }			//����� ��������� ����� �������
		virtual ~Freshman() { delete[] grades; }							//����������� ����������


		void set(std::string name) {
			this->name = name;
		}
		Freshman& operator= (Freshman&&);									//�������� =
		Freshman& operator= (const Freshman&);
		friend std::ostream& operator<< (std::ostream&, const Freshman&);	//�������� ������ <<

		virtual std::string toString() const;								//����� ��������� ������, ����������� ��� ������

		std::string getName() const { return name; }						//�������� ��� ��������
		void setGrades(int* grades);										//�������� ������
		double getAverageGrade() const;										//�������� ������� ���� ��������
		virtual bool getType() const { return true; }						//�������� ��� ��������
		bool isBad() const;													//���������� ������ ������ ������ ���� ����� 3
		virtual std::string toFile() const;									//�������� ������ ��� ������ � ����
	};

}
