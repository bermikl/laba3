#include "stdafx.h"
#include "senior.h"

#include <sstream>

using namespace lib;
using namespace std;

Senior::Senior(Senior&& S)
{
	name = S.name;
	count = S.count;
	grades = S.grades;
	S.grades = NULL;
	working = S.working;
	location = S.location;
	grade = S.grade;
}

Senior::Senior(const Senior& S)
{
	name = S.name;
	grades = new int[S.count];
	for (count = 0; count < S.count; count++)
		grades[count] = S.grades[count];
	working = S.working;
	location = S.location;
	grade = S.grade;
}

Senior& Senior::operator= (Senior&& S)
{
	if (this != &S)
	{
		name = S.name;
		count = S.count;
		grades = S.grades;
		S.grades = NULL;
		working = S.working;
		location = S.location;
		grade = S.grade;
	}
	return *this;
}

Senior& Senior::operator= (const Senior& S)
{
	if (this != &S)
	{
		name = S.name;
		grades = new int[S.count];
		for (count = 0; count < S.count; count++)
			grades[count] = S.grades[count];
		working = S.working;
		location = S.location;
		grade = S.grade;
	}
	return *this;
}


string Senior::toString() const
{
	int width = 20;
	ostringstream s;
	s.width(width);
	s << name;
	s.width(width);
	s << working;
	s.width(width);
	s << location;
	s.width(width / 2);
	s << grade << endl << "Grades: ";
	if (count != 0)
	{
		for (int i = 0; i < count; i++)
			s << grades[i] << " ";
		s << endl;
	}
	return s.str();
}

string Senior::toFile() const
{
	ostringstream s;
	s << name << endl;
	s << working << endl;
	s << location << endl;
	s << grade << endl;
	for (int i = 0; i < count; i++)
		s << grades[i] << endl;
	return s.str();

}