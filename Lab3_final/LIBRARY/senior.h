#pragma once
#include "freshman.h"

namespace lib
{

	class Senior :
		public Freshman
	{
	private:
		std::string working;														//���� ������
		std::string location;														//����� ���������� ������
		int grade;																	//������

	public:
		Senior() : Freshman(), working(""), location(""), grade(0) {}				//��������� ������������
		Senior(std::string name, int count, std::string working, std::string location, int grade) : Freshman(name, count), working(working), location(location), grade(grade > 0 ? grade : 0) {}
		Senior(std::string name, int count, std::string working, std::string location, int grade, int* grades) : Freshman(name, count, grades), working(working), location(location), grade(grade > 0 ? grade : 0) {}
		Senior(Senior&&);
		Senior(const Senior&);

		virtual Freshman* clone() { return new Senior(*this); }						//������������� ����� ������������, ��������� � �������� ����������� ������
		virtual ~Senior() {}														//����������� ����������

		Senior& operator= (Senior&&);												//���������=
		Senior& operator= (const Senior&);

		virtual std::string toString() const;										//������������� ����� ��������� ������ ��� ������
		std::string getWorking() const { return working; }							//�������� ����
		std::string getLocation() const { return location; }						//�����
		int getGrade() const { return grade; }										//������
		virtual bool getType() const { return false; }								//���
		void setWorking(std::string working) { Senior::working = working; }			//�������� ���� ������
		void setLocation(std::string location) { Senior::location = location; }		//�������� ����� ����������
		void setGrade(int grade) { Senior::grade = grade; }							//�������� ������
		virtual std::string toFile() const;											//�������� ������ ��� ������ � ����
	};

}
