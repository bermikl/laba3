#pragma once

#include "freshman.h"

#include <set>
#include <functional>
#include <string>
#include "Multiset.h"
#define multiset Multiset											//���� ��� ������ ����, �� ������������ ������������ Multiset, ���� ���������������, �� ������������ STL multiset

namespace lib
{

	class Group
	{
	private:
		typedef std::multiset < Freshman*, std::function<bool(Freshman*, Freshman*)>> mset;	//��� multiset, ������� ����� ���
		std::string name;											//��� ������
		int count;													//����� ������
		bool type;													//true - �������������, false - �������������
		mset students;												//multiset ���������

	public:
		class iterator;												//���� ����� ���������

		Group() : Group("", 0, true) {}								//������ �����������
		Group(std::string name, int count, bool type);				//����������� � ������� ������
		Group(Group&&);												//������������
		Group(const Group&);										//� ���������� ������������
		~Group();

		Group& operator= (Group&&);									//���������=
		Group& operator= (const Group&);
		friend std::ostream& operator<< (std::ostream&, const Group&);	//�����

		std::string getName() const { return name; }				//��������� ����� ������
		void add(std::string student);								//��������� �������� ���������� ��������
		void add(std::string student, int* grades);
		void add(std::string student, std::string working, std::string location, int grade);
		void add(std::string student, std::string working, std::string location, int grade, int* grades);
		void add(Freshman* student);
		int getStudentsCount(std::string name) const;				//��������� ����� ��������� � ����������� �������
																	//int getStudentsCount() const { return students.size(); }
		std::string printStudents(std::string name) const;			//������� ���� ��������� � �������� ������
		bool getType() const { return type; }						//�������� ��� ������
		Freshman* cut(std::string name, int index);					//�������� ��������
		unsigned int size() const { return students.size(); }		//�������� ������ ������
		unsigned int getCount() const { return count; }				//�������� ����� ������
		std::string printStudents(unsigned int index) const;		//����� �������� �� ������
		void setStudentGrades(unsigned index, int* grades);			//�������� ������ �������� �� ������
		double getAveradeGrade() const;								//�������� ������� ���� ������
		void change(std::string name, std::string working, std::string location, int grade, unsigned index);	//�������� ���������� � ��������������
		std::string toFile() const;									//�������� ������ ��� ������ � ����

		iterator begin() { return iterator(*this, students.begin()); }		//�������� ��������, ����������� �� ������
		iterator end() { return iterator(*this, students.end()); }			//�������� ��������, ����������� �� �����


																			//��������� ����� ���������
		class iterator
		{
		private:
			Group& G;												//������ �� ������
			mset::const_iterator cur;								//������� ��������� ���������
		public:
			iterator(Group& G, mset::const_iterator cur) : G(G), cur(cur) {}	//�����������

			iterator& operator++()									//++��������
			{
				++cur;
				return *this;
			}

			iterator operator++(int)								//��������++
			{
				iterator it(*this);
				++cur;
				return it;
			}

			bool operator!= (const iterator& I) const				//!=
			{
				return cur != I.cur;
			}

			Freshman* operator*()									//��������� ��������
			{
				return *cur;
			}
		};
	};
}
