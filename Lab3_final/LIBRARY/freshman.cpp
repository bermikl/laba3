#include "stdafx.h"
#include "freshman.h"

#include <sstream>


using namespace lib;
using namespace std;

Freshman::Freshman(string n, int c)
{
	if (c < 0)
		c = 0;
	name = n;
	grades = new int[c];
	for (count = 0; count < c; count++)
		grades[count] = 0;
}

Freshman::Freshman(string n, int c, int* g)
{
	if (c < 0)
		c = 0;
	name = n;
	grades = new int[c];
	for (count = 0; count < c; count++)
		grades[count] = g[count];
}

Freshman::Freshman(Freshman&& F)
{
	name = F.name;
	count = F.count;
	grades = F.grades;
	F.grades = NULL;
}

Freshman::Freshman(const Freshman& F)
{
	name = F.name;
	grades = new int[F.count];
	for (count = 0; count < F.count; count++)
		grades[count] = F.grades[count];
}

Freshman& Freshman::operator= (Freshman&& F)
{
	if (this != &F)
	{
		name = F.name;
		count = F.count;
		grades = F.grades;
		F.grades = NULL;
	}
	return *this;
}

Freshman& Freshman::operator= (const Freshman& F)
{
	if (this != &F)
	{
		name = F.name;
		grades = new int[F.count];
		for (count = 0; count < F.count; count++)
			grades[count] = F.grades[count];
	}
	return *this;
}

ostream& lib::operator<< (ostream& s, const Freshman& F)
{
	s << F.toString();
	return s;
}

string Freshman::toString() const
{
	int width = 20;
	ostringstream s;
	s.width(width);
	s << name << endl << "Grades: ";
	if (count != 0)
	{
		for (int i = 0; i < count; i++)
			s << grades[i] << " ";
		s << endl;
	}
	return s.str();
}

void Freshman::setGrades(int* g)
{
	for (int i = 0; i < count; i++)
		grades[i] = g[i];
}

double Freshman::getAverageGrade() const
{
	if (count == 0)
		return 0;
	double grade = 0;
	for (int i = 0; i < count; i++)
		grade += grades[i];
	return grade / count;
}

bool Freshman::isBad() const
{
	int index = 0;
	for (int i = 0; i < count; i++)
		if (grades[i] <= 2)
			index++;
	return index >= 3;
}

string Freshman::toFile() const
{
	ostringstream s;
	s << name << endl;
	for (int i = 0; i < count; i++)
		s << grades[i] << endl;
	return s.str();
}