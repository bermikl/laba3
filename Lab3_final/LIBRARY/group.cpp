#include "stdafx.h"
#include "group.h"
#include "senior.h"

#include <sstream>

using namespace lib;
using namespace std;


Group::Group(std::string n, int c, bool t)
{
	if (c < 0)
		c = 0;
	name = n;
	count = c;
	type = t;
	students = mset([](Freshman* f1, Freshman* f2) { return f1->getName() < f2->getName(); });
}

Group::Group(Group&& G)
{
	name = G.name;
	count = G.count;
	type = G.type;
	students = G.students;
	G.students = mset([](Freshman* f1, Freshman* f2) { return f1->getName() < f2->getName(); });
}

Group::Group(const Group& G)
{
	name = G.name;
	count = G.count;
	type = G.type;
	students = mset([](Freshman* f1, Freshman* f2) { return f1->getName() < f2->getName(); });
	for each(Freshman* freshman in G.students)
		students.insert(freshman->clone());
}

Group::~Group()
{
	for each (Freshman* freshman in students)
		delete freshman;
}


Group& Group::operator= (Group&& G)
{
	if (this != &G)
	{
		name = G.name;
		count = G.count;
		type = G.type;
		students = G.students;
		G.students = mset([](Freshman* f1, Freshman* f2) { return f1->getName() < f2->getName(); });
	}
	return *this;
}

Group& Group::operator= (const Group& G)
{
	if (this != &G)
	{
		name = G.name;
		count = G.count;
		type = G.type;
		students = mset([](Freshman* f1, Freshman* f2) { return f1->getName() < f2->getName(); });
		for each(Freshman* freshman in G.students)
			students.insert(freshman->clone());
	}
	return *this;
}

ostream& lib::operator<< (ostream& s, const Group& G)
{
	s << "Group \"" << G.name << "\", count = " << G.count << ", " << (G.type ? "Freashmen" : "Seniors") << endl;
	int width = 20;
	if (G.students.size() != 0)
	{
		s.width(width);
		s << "Name";
		if (!G.type)
		{
			s.width(width);
			s << "working";
			s.width(width);
			s << "location";
			s.width(width / 2);
			s << "grade";
		}
		s << endl;
		for each(Freshman* freshman in G.students)
			s << *freshman;
	}
	else
		s << "No students" << endl;
	return s;
}

void Group::add(string student)
{
	if (type)
		students.insert(new Freshman(student, count));
	else
		throw exception("Incorrect type of student");
}

void Group::add(std::string student, int* grades)
{
	if (type)
		students.insert(new Freshman(student, count, grades));
	else
		throw exception("Incorrect type of student");
}

void Group::add(string student, string working, string location, int grade)
{
	if (!type)
		students.insert(new Senior(student, count, working, location, grade));
	else
		throw exception("Incorrect type of student");
}

void Group::add(string student, string working, string location, int grade, int* grades)
{
	if (!type)
		students.insert(new Senior(student, count, working, location, grade, grades));
	else
		throw exception("Incorrect type of student");
}

int Group::getStudentsCount(string name) const
{
	int count = 0;
	Freshman F = Freshman(name, 1);
	return students.count(&F);
}

string Group::printStudents(string name) const
{
	ostringstream s;
	Freshman F = Freshman(name, 1);
	int i = 0;
	for (mset::const_iterator it = students.lower_bound(&F); it != students.upper_bound(&F); it++, i++)
		s << i << ' ' << *(*it);
	return s.str();
}

Freshman* Group::cut(std::string name, int index)
{
	Freshman F = Freshman(name, 1);
	mset::iterator it = students.lower_bound(&F);
	if (it == students.end() || (unsigned int)index >= students.count(&F) || index < 0)
		throw exception("Incorrect student");
	else
	{
		while (index)
		{
			++it;
			--index;
		}
		Freshman* student = *it;
		students.erase(it);
		return student;
	}
}

void Group::add(Freshman* student)
{
	if (type)
		add(student->getName());
	else
		if (typeid(*student) == typeid(Freshman))
			add(student->getName(), "-", "-", 0);
		else
			add(student->getName(), ((Senior*)student)->getWorking(), ((Senior*)student)->getLocation(), ((Senior*)student)->getGrade());
}

string Group::printStudents(unsigned int index) const
{
	if (index >= students.size())
		return "";
	ostringstream s;
	mset::const_iterator it = students.begin();
	while (index--)
		it++;
	s << **it;
	return s.str();
}

void Group::setStudentGrades(unsigned index, int* grades)
{
	if (index >= students.size())
		throw out_of_range("");
	mset::iterator it = students.begin();
	while (index--)
		it++;
	(*it)->setGrades(grades);
}

double Group::getAveradeGrade() const
{
	if (students.size() == 0)
		return 0;
	double grade = 0;
	for each(Freshman* student in students)
		grade += student->getAverageGrade();
	return grade / students.size();
}

void Group::change(std::string name, std::string working, std::string location, int grade, unsigned index)
{
	if (!type)
	{
		Freshman F = Freshman(name, 1);
		mset::iterator it = students.lower_bound(&F);
		if (it == students.end() || index >= students.count(&F))
			throw exception("Incorrect student");
		else
		{
			while (index)
			{
				++it;
				--index;
			}
			((Senior*)(*it))->setWorking(working);
			((Senior*)(*it))->setLocation(location);
			((Senior*)(*it))->setGrade(grade);
		}
	}
	else
		throw exception("Incorrect type of student");
}

string Group::toFile() const
{
	ostringstream s;
	s << getName() << endl;
	s << getCount() << endl;
	if (getType())
		s << 'f' << endl;
	else
		s << 's' << endl;
	s << size() << endl;
	for each(Freshman* f in students)
		s << f->toFile();
	return s.str();
}