#pragma once

#include <exception>
#include <functional>
//������ - Multiset (���������� multiset �� STL)

namespace std
{

	template <class T> struct Less : std::binary_function <T, T, bool>		//������ �������� ��������� �� ���������
	{
		bool operator() (const T& x, const T& y) const { return x < y; }
	};

	template <class T, class Compare = Less<T>>								//����� ���������� ������ ����� ��� ����� ������. ������ ���� - ��� (T) ��������� �������  
	class Multiset															//������ - ����� �������� �����������, �� ��������� Less
	{
	private:
		Compare comp;														//����������, ������������ � ������ �������
		T* items;															//��������	- �������� � �������
		unsigned int sz;													//������� ������

	public:

		class iterator;														//��������� ����� ���������
		class const_iterator;												//���������� ����� ������������ ���������

		Multiset();															//������ �����������
		Multiset(std::function<bool(T, T)>);								//����������� � ������� �����������
		~Multiset() { delete[] items; }										//����������
		Multiset(const Multiset<T, Compare>& M);							//���������� �����������
		Multiset(Multiset<T, Compare>&& M);									//������������ �����������

		Multiset<T, Compare>& operator= (const Multiset<T, Compare>& M);	//��������� =
		Multiset<T, Compare>& operator= (Multiset<T, Compare>&& M);
		bool operator != (const Multiset<T, Compare>& M) const { return M.items != items; }	//��������� ���� Multiset
		bool operator == (const Multiset<T, Compare>& M) const { return M.items == items; }

		unsigned int size() const { return sz; }							//��������� ���������� ���������
		unsigned int count(T) const;										//��������� ���������� ���������� ���������

		iterator begin() { return iterator(*this, 0); }						//���������� �������� �� ������
		iterator end() { return iterator(*this, sz); }						//���������� �������� �� �����

		const_iterator begin() const { return const_iterator(*this, 0); }	//���������� ����������� ���������
		const_iterator end() const { return const_iterator(*this, sz); }


		iterator insert(T t)												//�������, ���������� �������� �� ����� �������
		{
			unsigned int i = 0;
			T* itemsNew = new T[sz + 1];									//������������ ������
			for (; sz != i && !comp(t, items[i]); i++)						//���� ��������� ������� �� ������ 
				itemsNew[i] = items[i];
			itemsNew[i++] = t;												//��������� ��������
			iterator it = iterator(*this, i - 1);							//��������� �������� ��� ��������
			for (; i <= sz; i++)
				itemsNew[i] = items[i - 1];									//�������� ������ �����
			delete[] items;													//������� �������� ������
			items = itemsNew;												//���������� ���������
			sz++;															//����������� ������
			return it;
		}

		const_iterator lower_bound(const T& t) const						//��������� const_iterator, ����������� �� ������ ��������� t
		{
			unsigned int i = 0;
			for (Multiset<T, Compare>::const_iterator it = begin(); it != end(); it++)
				if (!comp(*it, t) && !comp(t, *it))
					return it;
			return end();													//���� ��������� �� ����, �� ��������� const_iterator, ����������� �� �����
		}

		iterator lower_bound(const T& t)									//���� �����, ������ ����� ������������� � ����������
		{																	//� ���������� ������������� ��������
			unsigned int i = 0;
			for (Multiset<T, Compare>::iterator it = begin(); it != end(); it++)
				if (!comp(*it, t) && !comp(t, *it))
					return it;
			return end();
		}

		const_iterator upper_bound(const T& t) const						//���������� const_iterator, ����������� �� ��������� ������� ����� 
		{																	//���������� ��������� t � Multiset
			const_iterator it = lower_bound(t);
			if (end() != it)
				while (!comp(*it, t) && !comp(t, *it))
					it++;
			return it;														//���� ��������� ���, ������� end()
		}

		iterator upper_bound(const T& t)									//���� ����� ��� �������������� ��������� iterator
		{
			iterator it = lower_bound(t);
			if (end() != it)
				while (!comp(*it, t) && !comp(t, *it))
					it++;
			return it;
		}

		void erase(iterator it)												//������� �������� �� ���������
		{
			if (it.M == *this && it.cur < sz)								//�������� ������������ ���������
			{
				unsigned int i = 0;
				T* itemsNew = new T[sz - 1];								//������������ ������
				for (i = 0; i < it.cur; i++)
					itemsNew[i] = items[i];
				for (i = it.cur + 1; i < sz; i++)							//������� ��������
					itemsNew[i - 1] = items[i];
				delete[] items;												//������� �������� ������
				items = itemsNew;											//���������� ���������
				sz--;														//��������� ������
			}
		}

		class iterator														//����� ��������� ���������� �������� STL, �������� ������� ����������
		{
			friend Multiset;												//�ultiset � ����������� ��������
		private:
			Multiset<T, Compare>& M;										//������ �� Multiset
			unsigned int cur;												//����� ��������
		public:
			iterator(Multiset<T, Compare>& M, unsigned int cur) : M(M), cur(cur) {}	//�o���������

			iterator& iterator::operator= (iterator& it)					//�������� =
			{
				cur = it.cur;
				M = it.M;
				return *this;
			}

			iterator& operator++()											//��������� ����������
			{
				++cur;
				return *this;
			}

			iterator operator++(int)										//�����������
			{
				iterator it(*this);
				++cur;
				return it;
			}

			iterator operator+(int i) {										//+ �����
				iterator it(*this);
				it.cur += i;
				return it;
			}

			bool operator!= (const iterator& I) const						// ��������� ����������
			{
				return M != I.M || cur != I.cur;
			}

			bool operator== (const iterator& I) const						//��������� ����������
			{
				return M == I.M && cur == I.cur;
			}


			T& operator*()													//�������� *, �������� ������������� ������ �� �
			{
				if (cur < 0 || cur >= M.size())
					throw out_of_range("");									//� ������ ������ �� ������� - ���������� ����������
				return M.items[cur];
			}
		};

		class const_iterator												//����������� ��������, �� ������ ��������� ������� Multiset
		{
			friend iterator;
		private:
			const Multiset<T, Compare>& M;									//����������� ������
			unsigned int cur;												//�������
		public:
			const_iterator(const Multiset<T, Compare>& M, unsigned int cur) : M(M), cur(cur) {}	//������������
			const_iterator(const iterator it) : M(it.M), cur(it.cur) {}		//����������� ���������� � �������� ���������
			const_iterator& operator++()									//����������
			{
				++cur;
				return *this;
			}

			const_iterator operator++(int)
			{
				const_iterator it(*this);
				++cur;
				return it;
			}

			const_iterator operator+(int i) {
				const_iterator it(*this);
				it.cur += i;
				return it;
			}

			bool operator!= (const const_iterator& I) const					//���������
			{
				return M != I.M || cur != I.cur;
			}

			bool operator== (const const_iterator& I) const
			{
				return M == I.M || cur == I.cur;
			}

			const T& operator*()											//�������� *
			{
				if (cur < 0 || cur >= M.size())
					throw out_of_range("");
				return M.items[cur];
			}
		};


	};

	//���������� ��������� ������� Multiset, ����������� ������� ������

	template <class T, class Compare = Less<T>>
	Multiset<T, Compare>::Multiset()										//������ ����������� Multiset
	{
		comp = Less<T>();
		sz = 0;
		items = new T[0];
	}

	template <class T, class Compare = Less<T>>
	Multiset<T, Compare>::Multiset(std::function<bool(T, T)> f)				//����������� �������� (������-���������)
	{
		comp = f;
		sz = 0;
		items = new T[0];
	}

	template <class T, class Compare = Less<T>>
	Multiset<T, Compare>::Multiset(const Multiset<T, Compare>& M)			//���������� �����������
	{
		comp = M.comp;
		items = new T[M.sz];
		for (sz = 0; sz < M.size(); sz++)
			items[sz] = M.items[sz];
	}

	template <class T, class Compare = Less<T>>
	Multiset<T, Compare>::Multiset(Multiset<T, Compare>&& M)				//������������
	{
		comp = M.comp;
		items = M.items;
		sz = M.sz;
		M.items = NULL;
	}

	template <class T, class Compare = Less<T>>
	Multiset<T, Compare>& Multiset<T, Compare>::operator= (const Multiset<T, Compare>& M)	//�������� = ����������
	{
		if (this != &M)
		{
			comp = M.comp;
			items = new T[M.sz];
			for (sz = 0; sz < M.size(); sz++)
				items[sz] = M.items[sz];
		}
		return *this;
	}

	template <class T, class Compare = Less<T>>
	Multiset<T, Compare>& Multiset<T, Compare>::operator= (Multiset<T, Compare>&& M)		//�������� = ������������
	{
		if (this != &M)
		{
			comp = M.comp;
			items = M.items;
			sz = M.sz;
			M.items = NULL;
		}
		return *this;
	}

	template <class T, class Compare = Less<T>>
	unsigned int  Multiset<T, Compare>::count(T t) const					//����� ���������
	{
		unsigned int count = 0;
		for (unsigned int i = 0; i < sz; i++)
			if (!comp(items[i], t) && !comp(t, items[i]))
				count++;
		return count;
	}

}
