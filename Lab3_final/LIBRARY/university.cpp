#include "stdafx.h"
#include "university.h"

#include <sstream>
#include <fstream>

using namespace std;
using namespace lib;

University::University(University&& U)												//������������
{
	groups = U.groups;
	U.groups = vector<Group*>();
}

University::University(const University& U)
{
	groups = vector<Group*>();
	for each (Group* group in U.groups)
		groups.push_back(new Group(*group));
}

University::~University()															//����������
{
	for each(Group* group in groups)
		delete group;
}

University& University::operator= (University&& U)									//��������� =
{
	if (this != &U)
	{
		groups = U.groups;
		U.groups = vector<Group*>();
	}
	return *this;
}

University& University::operator= (const University& U)
{
	if (this != &U)
	{
		groups = vector<Group*>();
		for each (Group* group in U.groups)
			groups.push_back(new Group(*group));
	}
	return *this;
}

ostream& lib::operator<< (ostream& s, const University& U)							//�������� ������
{
	for each(Group* group in U.groups)
		s << *group << endl;
	return s;
}

Group* University::find(string name) const											//�����
{
	for each(Group* group in groups)												//���������� ��������, ���� �� �����
		if (group->getName() == name)
			return group;
	return NULL;
}

void University::addGroup(string name, int count, bool type)						//�������� ������
{
	if (count <= 0)
		throw exception("Incorrect count of grades");								//�������� �� ���������� ������
	if (!find(name))
		groups.push_back(new Group(name, count, type));								//��������� ������ � �����
	else
		throw exception("Group exist.");
}


void University::addStudent(string group, string name)								//�������� �������� ��������
{
	Group* G = find(group);
	if (G)
		G->add(name);
	else
		throw exception("Group not found");
}

void University::addStudent(string group, string name, int* grades)					//�������� ������� � ��������
{
	Group* G = find(group);
	if (G)
		G->add(name, grades);
	else
		throw exception("Group not found");
}


void University::addStudent(string group, string name, string working, string location, int grade)	//��������������
{
	Group* G = find(group);
	if (G)
		G->add(name, working, location, grade);
	else
		throw exception("Group not found");
}

void University::addStudent(string group, string name, string working, string location, int grade, int* grades)	//�������������� � ��������
{
	Group* G = find(group);
	if (G)
		G->add(name, working, location, grade, grades);
	else
		throw exception("Group not found");
}

int University::getStudentsCount(string name, string group) const					//����� ������ ��������� � ������
{
	Group* G = find(group);
	if (G)
		return G->getStudentsCount(name);
	else
		return -1;
}

string University::printStudents(string name, string group) const					//����� ������ ���������
{
	Group* G = find(group);
	if (G)
		return G->printStudents(name);
	else
		return "";
}

void University::changeGroup(string name, string groupOld, string groupNew, int index)	//�������� ������
{
	Group* gOld = find(groupOld);													//����� �����
	Group* gNew = find(groupNew);
	if (gOld != NULL && gNew != NULL)
	{																				//� ������ ��������� ������
		Freshman* student = gOld->cut(name, index);
		gNew->add(student);
		delete student;
	}
	else
		throw exception("Group not found");
}

unsigned int University::sizeGroup(string group) const								//����� ����� ��������� � ������
{
	Group* G = find(group);
	if (G != NULL)
		return G->size();
	else
		throw exception("Group not found");
}

unsigned int University::getGroupCountGrades(string group) const					//�������� ����� ��������� � ������
{
	Group* G = find(group);
	if (G != NULL)
		return G->getCount();
	else
		throw exception("Group not found");
}

string University::printStudents(unsigned int index, string group) const			//����� ���������� ������� �� ������
{
	Group* G = find(group);
	if (G)
		return G->printStudents(index);
	else
		return "";
}

void University::setStudentGrades(std::string group, unsigned int index, int* grades)	//��������� ������ � ������ ��� ��������
{
	Group* G = find(group);
	if (G)
		return G->setStudentGrades(index, grades);
	else
		throw exception("Group not found");
}

string University::printGroup(string group) const									//����� ����� ������ ���������
{
	Group* G = find(group);
	ostringstream s;
	if (G)
	{
		s << *G;
		return s.str();
	}
	else
		throw exception("Group not found");
}

double University::getAverageGrade(string group) const								//��������� �������� �����
{
	Group* G = find(group);
	if (G)
		return G->getAveradeGrade();
	else
		throw exception("Group not found");
}

void University::changeInformation(string group, string name, string working, string location, int grade, unsigned int index)	//��������� ��������������
{
	Group* G = find(group);
	if (G)
		G->change(name, working, location, grade, index);
	else
		throw exception("Group not found");
}

string University::getBadMarks(string group) const										//��������� ���������� �� �������
{
	ostringstream s;
	s << "";
	Group* G = find(group);
	if (G)
		for (Group::iterator it = G->begin(); it != G->end(); it++)
			if ((*it)->isBad())
				s << **it;
			else;
	else
		throw exception("Group not found");
	return s.str();
}

void University::save(string file) const												//���������� � ����
{
	ofstream s;
	s.open(file);
	for each (Group* g in groups)
		s << g->toFile();																//���������� � ���� ������ ������
	s.close();
}

void University::read(string file)														//������ �� �����
{
	clear();
	ifstream s;
	s.open(file);
	if (!s)
		throw exception("file not found");
	string groupName = "";
	int count = 0;
	char ch = 't';
	int studentsCount = 0;
	string name = "";
	string working = "";
	string location = "";
	int grade = 0;
	while (!s.eof())
	{
		try {
			s >> groupName >> count >> ch >> studentsCount;								//������ ������ ������
			addGroup(groupName, count, ch == 'f' || ch == 'F');
			for (int i = 0; i < studentsCount; i++)
				if (ch == 'f' || ch == 'F')
				{
					s >> name;															//� ���������
					int* grades = new int[count];
					for (int j = 0; j < count; j++)										//� �� ������
						s >> grades[j];
					addStudent(groupName, name, grades);
					delete[] grades;
				}
				else
				{
					s >> name >> working >> location >> grade;
					int* grades = new int[count];
					for (int j = 0; j < count; j++)
						s >> grades[j];
					addStudent(groupName, name, working, location, grade, grades);
					delete[] grades;
				}
		}
		catch (...)
		{

		}
	}
}

void University::clear()
{
	for each(Group* group in groups)													//������� ��� ������
		delete group;
	groups.clear();
}