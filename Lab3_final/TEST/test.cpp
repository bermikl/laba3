#include "stdafx.h"
#include "..\LIBRARY\university.h"
#include "..\LIBRARY\freshman.h"
#include "..\LIBRARY\senior.h"
#include "..\LIBRARY\group.h"
#include "..\LIBRARY\Multiset.h"
#include "gtest\gtest.h"

using namespace std;
using namespace lib;

TEST(LibTesting, CheckFreshman)								//���� ������ Freshman
{
	Freshman F1 = Freshman();
	ASSERT_STREQ(F1.getName().c_str(), "");
	ASSERT_DOUBLE_EQ(F1.getAverageGrade(), 0);
	ASSERT_TRUE(F1.getType());

	Freshman F2 = Freshman("st", 2);
	ASSERT_STREQ(F2.getName().c_str(), "st");
	ASSERT_DOUBLE_EQ(F1.getAverageGrade(), 0);

	int* grades = new int[3];
	grades[0] = 1;
	grades[1] = 3;
	grades[2] = 2;
	Freshman F = Freshman("st2", 3, grades);
	ASSERT_STREQ(F.toString().c_str(), "                 st2\nGrades: 1 3 2 \n");
	ASSERT_STREQ(F.getName().c_str(), "st2");
	ASSERT_DOUBLE_EQ(F.getAverageGrade(), 2);
	ASSERT_TRUE(F.getType());
	ASSERT_FALSE(F.isBad());
	ASSERT_STREQ(F.toFile().c_str(), "st2\n1\n3\n2\n");
	int* grades2 = new int[3];
	grades2[0] = 2;
	grades2[1] = 5;
	grades2[2] = 2;
	F.setGrades(grades2);

	Freshman F3 = F;
	ASSERT_STREQ(F3.toString().c_str(), "                 st2\nGrades: 2 5 2 \n");
	ASSERT_STREQ(F3.getName().c_str(), "st2");
	ASSERT_DOUBLE_EQ(F3.getAverageGrade(), 3);
	ASSERT_TRUE(F3.getType());
	ASSERT_FALSE(F3.isBad());
	ASSERT_STREQ(F3.toFile().c_str(), "st2\n2\n5\n2\n");
	Freshman F4 = Freshman(F3);
	F3.setGrades(grades);
	ASSERT_STREQ(F4.toFile().c_str(), "st2\n2\n5\n2\n");
}

TEST(LibTesting, CheckSenior)								//���� ������ Senior
{
	Senior F1 = Senior();
	ASSERT_STREQ(F1.getName().c_str(), "");
	ASSERT_STREQ(F1.getWorking().c_str(), "");
	ASSERT_STREQ(F1.getLocation().c_str(), "");
	ASSERT_EQ(F1.getGrade(), 0);
	ASSERT_DOUBLE_EQ(F1.getAverageGrade(), 0);
	ASSERT_FALSE(F1.getType());

	Senior F2 = Senior("st", 2, "work", "locat", 2);
	ASSERT_STREQ(F2.getName().c_str(), "st");
	ASSERT_STREQ(F2.getWorking().c_str(), "work");
	ASSERT_STREQ(F2.getLocation().c_str(), "locat");
	ASSERT_EQ(F2.getGrade(), 2);
	ASSERT_DOUBLE_EQ(F1.getAverageGrade(), 0);

	int* grades = new int[3];
	grades[0] = 1;
	grades[1] = 3;
	grades[2] = 2;
	Senior F = Senior("st2", 3, "work", "locat", 2, grades);
	ASSERT_STREQ(F.toString().c_str(), "                 st2                work               locat         2\nGrades: 1 3 2 \n");
	ASSERT_STREQ(F.getName().c_str(), "st2");
	ASSERT_STREQ(F2.getWorking().c_str(), "work");
	ASSERT_STREQ(F2.getLocation().c_str(), "locat");
	ASSERT_DOUBLE_EQ(F.getAverageGrade(), 2);
	ASSERT_FALSE(F.getType());
	ASSERT_FALSE(F.isBad());
	ASSERT_STREQ(F.toFile().c_str(), "st2\nwork\nlocat\n2\n1\n3\n2\n");
	int* grades2 = new int[3];
	grades2[0] = 2;
	grades2[1] = 2;
	grades2[2] = 2;
	F.setGrades(grades2);
	Senior F3 = F;
	ASSERT_STREQ(F3.toString().c_str(), "                 st2                work               locat         2\nGrades: 2 2 2 \n");
	ASSERT_STREQ(F3.getName().c_str(), "st2");
	ASSERT_DOUBLE_EQ(F3.getAverageGrade(), 2);
	ASSERT_FALSE(F3.getType());
	ASSERT_TRUE(F3.isBad());
	ASSERT_STREQ(F3.toFile().c_str(), "st2\nwork\nlocat\n2\n2\n2\n2\n");
	Senior F4 = Senior(F3);
	F3.setGrades(grades);
	ASSERT_STREQ(F4.toFile().c_str(), "st2\nwork\nlocat\n2\n2\n2\n2\n");
}

TEST(LibTesting, CheckGroup)				//���� group
{
	Group G = Group("gr1", 5, true);
	G.add("st1");
	G.add("st2");
	G.add("st1");
	ASSERT_STREQ(G.getName().c_str(), "gr1");
	ASSERT_EQ(G.size(), 3);
	ASSERT_EQ(G.getStudentsCount("st1"), 2);
	ASSERT_EQ(G.getStudentsCount("st2"), 1);
	ASSERT_EQ(G.getCount(), 5);
	ASSERT_TRUE(G.getType());
	ASSERT_EQ(G.getAveradeGrade(), 0);
	Group::iterator it = G.begin();
	ASSERT_STREQ((*it)->getName().c_str(), "st1");
	it++;
	ASSERT_STREQ((*it)->getName().c_str(), "st1");
	it++;
	ASSERT_STREQ((*it)->getName().c_str(), "st2");
	it++;
	ASSERT_FALSE(it != G.end());

}



//���� �������

TEST(MultisetTesting, CheckConstructor)				//���� �������������
{
	Multiset <int> M = Multiset <int>();
	ASSERT_EQ(M.size(), 0);
	Multiset <int, std::function<bool(int, int)>> M2 = Multiset <int, std::function<bool(int, int)>>([](int x, int y) { return x < y; });
	ASSERT_EQ(M2.size(), 0);
	Multiset <int, std::function<bool(int, int)>> M3 = M2;
	ASSERT_EQ(M3.size(), 0);
	Multiset <int, std::function<bool(int, int)>> M4 = Multiset <int, std::function<bool(int, int)>>(M2);
	ASSERT_EQ(M4.size(), 0);
	Multiset <int, std::function<bool(int, int)>> M5 = std::move(M4);
	ASSERT_EQ(M5.size(), 0);
	Multiset <pair<int, int>, function<bool(pair<int, int>, pair<int, int>)>> mset = Multiset <pair<int, int>, function<bool(pair<int, int>, pair<int, int>)>>([](pair<int, int> p1, pair <int, int> p2) {return p1.first < p2.first; });
	mset.insert(pair<int, int>(3, 2));
	mset.insert(pair<int, int>(2, 2));
	mset.insert(pair<int, int>(1, 1));
	ASSERT_EQ(mset.size(), 3);
	Multiset <pair<int, int>, function<bool(pair<int, int>, pair<int, int>)>> mset2 = Multiset <pair<int, int>, function<bool(pair<int, int>, pair<int, int>)>>(mset);
	ASSERT_EQ(mset2.size(), 3);
	Multiset <pair<int, int>, function<bool(pair<int, int>, pair<int, int>)>>::iterator it = mset.begin();
	Multiset <pair<int, int>, function<bool(pair<int, int>, pair<int, int>)>>::iterator it2 = mset2.begin();
	for (int i = 1; i < 4; i++)
	{
		ASSERT_EQ((*it).first, (*it2).first);
		ASSERT_EQ((*it).first, i);
		it++;
		it2++;
	}
}

TEST(MultisetTesting, CheckInsertErase)					//���� ������� � ��������
{
	Multiset <pair<int, int>, function<bool(pair<int, int>, pair<int, int>)>> mset = Multiset <pair<int, int>, function<bool(pair<int, int>, pair<int, int>)>>([](pair<int, int> p1, pair <int, int> p2) {return p1.first < p2.first; });
	ASSERT_FALSE(mset.insert(pair<int, int>(3, 2)) != mset.begin());
	ASSERT_FALSE(mset.insert(pair<int, int>(2, 2)) != mset.begin());
	ASSERT_FALSE(mset.insert(pair<int, int>(2, 1)) != ++mset.begin());
	ASSERT_FALSE(++mset.insert(pair<int, int>(3, 7)) != ++mset.end());
	ASSERT_FALSE(mset.insert(pair<int, int>(0, 10)) != mset.begin());
	ASSERT_FALSE(mset.insert(pair<int, int>(1, 5)) != ++mset.begin());
	ASSERT_FALSE(mset.insert(pair<int, int>(1, 4)) != ++++mset.begin());
	ASSERT_FALSE(mset.insert(pair<int, int>(0, 3)) != ++mset.begin());
	Multiset <pair<int, int>, function<bool(pair<int, int>, pair<int, int>)>>::iterator it = mset.begin();
	for (int i = 0; i < 3; i++)
	{
		ASSERT_EQ((*it).first, i);
		it++;
		ASSERT_EQ((*it).first, i);
		it++;
	}
	ASSERT_EQ(mset.count(pair<int, int>(1, 2)), 2);
	ASSERT_EQ(mset.size(), 8);
	it = mset.begin();
	mset.erase(it);
	it++;
	mset.erase(it);
	it++;
	mset.erase(it);
	it++;
	mset.erase(it);
	it++;
	ASSERT_FALSE(it != mset.end());
	it = mset.begin();
	for (int i = 0; i < 3; i++)
	{
		ASSERT_EQ((*it).first, i);
		it++;
	}
}

TEST(MultisetTesting, CheckIterator)								//���� ����������
{
	Multiset <int> M = Multiset <int>();
	Multiset <int>::iterator itb = M.begin();
	Multiset <int>::iterator ite = M.end();
	ASSERT_FALSE(itb != ite);
	ASSERT_THROW(*itb, out_of_range);
	itb++;
	ASSERT_THROW(*itb, out_of_range);
	const Multiset <int> M2 = Multiset <int>();
	Multiset <int>::const_iterator it2b = M2.begin();
	ASSERT_FALSE(it2b != M2.end());
	Multiset <int, std::function<bool(int, int)>> M3 = Multiset <int, std::function<bool(int, int)>>([](int x, int y) { return x < y; });
	Multiset <int, std::function<bool(int, int)>>::iterator it3 = M3.begin();
	ASSERT_FALSE(it3 != M3.end());
	M.insert(3);
	M.insert(1);
	M.insert(2);
	M.insert(4);
	M.insert(0);
	M.insert(1);
	M.insert(1);
	M.insert(0);
	ASSERT_EQ(M.count(1), 3);
	ASSERT_FALSE(++++M.begin() != M.lower_bound(1));
	ASSERT_FALSE(++++++M.lower_bound(1) != M.upper_bound(1));
	const Multiset <int> N = Multiset <int>(M);
	ASSERT_EQ(N.count(1), 3);
	ASSERT_FALSE(++++N.begin() != N.lower_bound(1));
	ASSERT_FALSE(++++++N.lower_bound(1) != N.upper_bound(1));
}

TEST(MultisetTesting, ClockSize)
{
	typedef std::Multiset < Freshman*, std::function<bool(Freshman*, Freshman*)>>mset;
	const int count = 10000;
	mset set = mset([](Freshman* f1, Freshman *f2) { return f1->getName() < f2->getName(); });
	for (int i = 0; i < count; i++) {
		set.insert(new Freshman());
	}

	time_t t1 = clock();
	set.size();
	time_t t2= clock();
	time_t t = t2 - t1;
	cout << t << endl;

	time_t q1 = clock();
	for (auto it = set.begin(); it != set.end(); it++) {

	}
	time_t q2 = clock();
	time_t q = q2 - q1;
	cout << q << endl;


	time_t r1 = clock();
	for (auto it = set.begin(); it != set.end(); it++) {
		(**it).set("q");
	}
	time_t r2 = clock();
	time_t r = r2 - r1;
	cout << r << endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	int point;
	::testing::InitGoogleTest(&argc, argv);
	point = RUN_ALL_TESTS();
	system("pause");
	return point;
}